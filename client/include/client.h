/*
** server.h for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:27:07 2014 camill_n
** Last update Tue Jun  3 00:07:35 2014 camill_n
*/

#ifndef SCARITY_H_
# define SCARITY_H_

# define SCARITY_PORT 2500
# define SOCK data->sock

// ---- FONCTIONS D'INIT ---- //

void	init_data(t_data *data);
void	load_map(t_data *data, char *path);

// ---- FONCTION GAMER ---- //

t_gamer	*add_gamer(t_data *data, char *pseudo);
void	del_gamer(t_data *data, t_gamer *gamer);
int	create_pgamer(t_data *data, t_send *send, t_gamer *gamer);
int	get_pgamer(t_data *data, t_paquet *paquet);

// ---- FONCTION DISPLAY ---- //

void	disp_gamer(t_data *data);
void	disp_sock(t_data *data);
void	disp_map(t_data *data);
void	disp_buff(t_data *data);
void	disp_paquet(t_data *data);

// ---- FONCTION RECEIVE DE PACKET ---- //

void	receive_req(t_data *data, t_sock *client);

void	init_pfunc(t_data *data);
int	auth(t_data *data, t_paquet *paquet);
int	deauth(t_data *data);
int	send_move(t_data *data, int mov);
int	connect_server(t_data *data, char *sev);
int	move_action(t_data *data, t_gamer *gamer, int mode);
int     request_map(t_data *data);
int     get_map(t_data *data, t_paquet *paquet);

#endif
