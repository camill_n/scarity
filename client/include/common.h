/*
** common.h for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:31:55 2014 camill_n
** Last update Sun Jun  1 01:20:33 2014 camill_n
*/

#ifndef COMMON_H_
# define COMMON_H_

#include <SDL/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include "wordtab.h"
#include "gui.h"
#include "struct.h"
#include "client.h"
#include "sock.h"
#include "header.h"
#include "thread.h"

#endif
