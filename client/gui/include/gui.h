/*
** gui.h for scarity_gui in /home/camill_n/scaritysave/client/gui
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 31 01:02:07 2014 camill_n
** Last update Sun Jun  1 02:06:54 2014 camill_n
*/

#ifndef GUI_H_
# define GUI_H_

# define WWIDTH		1600
# define WHEIGHT	900
# define WINDOW		data->window
# define REAL_X_COEF	20
# define REAL_Y_COEF	20

typedef struct s_data	t_data;

typedef struct		s_sprite
{
  char			*name;
  int			t;
  SDL_Surface		**img;
  struct s_sprite	*next;
}			t_sprite;

int		sdl_init(t_data *data);
int		sdl_loop(t_data *data);
void		putPixel(SDL_Surface *surface, Uint16 x, Uint16 y, Uint32 color);
int		load_sprite(t_data *data, char *n_sprite, char *path,
		    int t, int w, int h);
t_sprite	*add_sprite(t_data *data, char *name);
void		del_sprite(t_data *data, t_sprite *sprite);
void		init_sprite(t_data *data);
void		update_rec_gamer(t_data *data);
t_sprite	*get_sprite(t_data *data, char *name_sprite);
void		update_gamer(t_data *data);

#endif
