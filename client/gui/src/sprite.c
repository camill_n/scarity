/*
** sprite.c for scarity_gui in /home/camill_n/scaritysave/client
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Jun  1 01:13:30 2014 camill_n
** Last update Sun Jun  1 02:05:27 2014 camill_n
*/

#include "common.h"

void putPixel(SDL_Surface *surface, Uint16 x, Uint16 y, Uint32 color)
{
  if (x > 0 && x < WIDTH && y > 0 && y < HEIGHT)
    {
      /* Nombre de bits par pixels de la surface d'écran */
      Uint8 bpp = surface->format->BytesPerPixel;
      /* Pointeur vers le pixel à remplacer (pitch correspond à la taille
	 d'une ligne d'écran, c'est à dire (longueur * bitsParPixel)
	 pour la plupart des cas) */
      Uint8 * p = ((Uint8 *)surface->pixels) + y * surface->pitch + x * bpp;
      switch(bpp)
        {
	case 1:
	  *p = (Uint8) color;
	  break;
	case 2:
	  *(Uint16 *)p = (Uint16) color;
	  break;
	case 3:
	  if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
	    {
	      *(Uint16 *)p = ((color >> 8) & 0xff00) | ((color >> 8) & 0xff);
	      *(p + 2) = color & 0xff;
	    }
	  else
	    {
	      *(Uint16 *)p = color & 0xffff;
	      *(p + 2) = ((color >> 16) & 0xff) ;
	    }
	  break;
	case 4:
	  *(Uint32 *)p = color;
	  break;
        }
    }
}

int		load_sprite(t_data *data, char *path, char *n_sprite, int t, int w, int h)
{
  int		i = 0;
  SDL_Rect	pos_s;
  SDL_Rect	pos_d;
  t_sprite	*sprite;
  SDL_Surface	*pSprite = SDL_LoadBMP(path);

  sprite = add_sprite(data, n_sprite);
  sprite->img = malloc(t * sizeof(SDL_Surface *));
  sprite->t = t;
  while (i < t)
    {
      sprite->img[i] = SDL_CreateRGBSurface(SDL_HWSURFACE,
					    w, h, 32, 0, 0, 0,0);
      SDL_SetColorKey(sprite->img[i], SDL_SRCCOLORKEY,
		      SDL_MapRGB(WINDOW->format, 255, 255, 255));
      pos_s.x = i * w;
      pos_s.y = 0;
      pos_s.w = w;
      pos_s.h = h;
      pos_d.x = 0;
      pos_d.y = 0;
      SDL_BlitSurface(pSprite, &pos_s, sprite->img[i], &pos_d);
      ++i;
    }
  return (0);
}

t_sprite		*add_sprite(t_data *data, char *name)
{
  t_sprite		*tmp;
  t_sprite		*new_sprite;

  new_sprite = malloc(sizeof(t_sprite));
  new_sprite->next = NULL;
  new_sprite->name = strdup(name);
  if (data->sprite == NULL)
    data->sprite = new_sprite;
  else
    {
      tmp = data->sprite;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_sprite;
    }
  return (new_sprite);
}

void		del_sprite(t_data *data, t_sprite *sprite)
{
  t_sprite	*tmp;

  if (!data->sprite)
    return ;
  if (data->sprite != NULL && data->sprite->next == NULL)
    data->sprite = NULL;
  else if (data->sprite == sprite)
    data->sprite = sprite->next;
  else
    {
      tmp = data->sprite;
      while (tmp->next != sprite)
	tmp = tmp->next;
      if (sprite->next != NULL)
	tmp->next = sprite->next;
      else
	tmp->next = NULL;
    }
  free(sprite);
}

t_sprite	*get_sprite(t_data *data, char *name_sprite)
{
  t_sprite	*sprite;

  sprite = data->sprite;
  while (sprite)
    {
      if (!strcmp(sprite->name, name_sprite))
	return (sprite);
      sprite = sprite->next;
    }
  return (NULL);
}

void		init_sprite(t_data *data)
{
  load_sprite(data, "gui/img/gamer.bmp", "gamer", 16, 30, 38);
}
