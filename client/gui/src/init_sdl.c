/*
** init_sdl.c for scarity_gui in /home/camill_n/scaritysave/client/gui
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 31 01:01:49 2014 camill_n
** Last update Sat May 31 01:11:25 2014 camill_n
*/

#include "common.h"

int	sdl_init(t_data *data)
 {
   if (SDL_Init(SDL_INIT_VIDEO) < 0)
     {
       printf("Unable to init SDL: %s\n", SDL_GetError());
       return (1);
     }
   WINDOW= SDL_SetVideoMode(WWIDTH, WHEIGHT, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
   if (!WINDOW)
     {
       printf("Unable to set %dx%d video: %s\n",
	      WWIDTH, WHEIGHT, SDL_GetError());
       return (-1);
     }
   return (0);
 }
