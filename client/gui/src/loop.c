/*
** loop.c for scarity_gui in /home/camill_n/scaritysave/client
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 31 01:19:32 2014 camill_n
** Last update Tue Jun  3 00:08:24 2014 camill_n
*/

#include "common.h"

int	manage_view(t_data *data, SDL_Rect *background, SDL_Surface *bmp)
{
  SDL_FillRect(WINDOW, 0, SDL_MapRGB(WINDOW->format, 0, 0, 0));
  SDL_BlitSurface(bmp, 0, WINDOW, background);
  update_gamer(data);
  SDL_Flip(WINDOW);
  return (0);
}

int	sdl_loop(t_data *data)
{
  SDL_Surface *bmp = SDL_LoadBMP("gui/img/background.bmp");

  atexit(SDL_Quit);
  SDL_EnableKeyRepeat(10, 10);
  if (!bmp)
    {
      printf("Unable to load bitmap: %s\n", SDL_GetError());
      return 1;
    }
  SDL_Rect dstrect;
  dstrect.x = (WINDOW->w - bmp->w) / 2;
  dstrect.y = (WINDOW->h - bmp->h) / 2;
  while (data->run)
    {
      // message processing loop
      SDL_Event event;
      while (SDL_PollEvent(&event))
        {
	  // check for messages
	  switch (event.type)
            {
	      // exit if the window is closed
            case SDL_QUIT:
	      {
		deauth(data);
		--data->run;
	      }
	      break;

	      // check for keypresses
            case SDL_KEYDOWN:
	      {
		// exit if ESCAPE is pressed
		if (event.key.keysym.sym == SDLK_ESCAPE)
		  {
		    deauth(data);
		    --data->run;
		  }
		if (event.key.keysym.sym == SDLK_SPACE)
		  request_map(data);
		if (event.key.keysym.sym == SDLK_UP)
		  send_move(data, 0);
		if (event.key.keysym.sym == SDLK_DOWN)
		  send_move(data, 1);
		if (event.key.keysym.sym == SDLK_LEFT)
		  send_move(data, 2);
		if (event.key.keysym.sym == SDLK_RIGHT)
		  send_move(data, 3);
	      }
	      /* case SDL_MOUSEMOTION: */
	      /*   { */
	      /* 	SDL_GetMouseState(&mouse_pos[0], &mouse_pos[1]); */
	      /* 	if (mouse_pos[0] - 300 >= 0) */
	      /* 	  update_target(data, mouse_pos[0] - 300, mouse_pos[1]); */
	      /*   } */
	      /* case SDL_MOUSEBUTTONDOWN: */
	      /*   { */
	      /* 	// Clic gauche => tir */
	      /* 	if (event.button.button == SDL_BUTTON_LEFT) */
	      /* 	  add_event(data, ETIR0, 0, STIR0, TIR0_TIME, GAMER[0]->X, GAMER[0]->Y); */
	      /* 	if (event.button.button == SDL_BUTTON_RIGHT) */
	      /* 	  add_event(data, ETIR0, 0, STIR0, TIR0_TIME + 13, GAMER[0]->X, GAMER[0]->Y); */
	      /*   } */
            } // end switch
        }
      manage_view(data, &dstrect, bmp);
    }
  SDL_FreeSurface(bmp);
  printf("Exited cleanly\n");
  return (0);
}
