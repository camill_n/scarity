/*
** gamer.c for scarity_gui in /home/camill_n/scaritysave/client
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Jun  1 01:47:33 2014 camill_n
** Last update Sun Jun  1 02:07:39 2014 camill_n
*/

#include "common.h"

inline void	update_rec_gamer(t_data *data)
{
  t_gamer	*gamer;

  gamer = data->gamer;
  while (gamer)
    {
      gamer->real_pos.x = REAL_X_COEF * gamer->pos[0];
      gamer->real_pos.y = REAL_Y_COEF * gamer->pos[1];
      gamer = gamer->next;
    }
}

void	update_gamer(t_data *data)
{
  t_gamer	*gamer;

  gamer = data->gamer;
  update_rec_gamer(data);
  while (gamer)
    {
      SDL_BlitSurface(gamer->img, 0, WINDOW, &(gamer->real_pos));
      gamer = gamer->next;
    }
}
