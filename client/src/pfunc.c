/*
** pfunc.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed May 28 12:14:50 2014 camill_n
** Last update Tue Jun  3 00:06:22 2014 camill_n
*/

#include "common.h"

t_pfunc	*add_pfunc(t_data *data, char *cmd,
		   int (*exec)(t_data *data, t_paquet *paquet))
{
  t_pfunc	*tmp;
  t_pfunc	*new_pfunc;

  new_pfunc = malloc(sizeof(t_pfunc));
  new_pfunc->cmd = strdup(cmd);
  new_pfunc->exec = exec;
  new_pfunc->next = NULL;
  if (data->pfunc == NULL)
    data->pfunc = new_pfunc;
  else
    {
      tmp = data->pfunc;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_pfunc;
    }
  return (new_pfunc);
}

void		del_pfunc(t_data *data, t_pfunc *pfunc)
{
  t_pfunc	*tmp;

  if (!data->pfunc)
    return ;
  if (data->pfunc != NULL && data->pfunc->next == NULL)
    data->pfunc = NULL;
  else if (data->pfunc == pfunc)
    data->pfunc = pfunc->next;
  else
    {
      tmp = data->pfunc;
      while (tmp->next != pfunc)
	tmp = tmp->next;
      if (pfunc->next != NULL)
	tmp->next = pfunc->next;
      else
	tmp->next = NULL;
    }
  free(pfunc);
}

void		init_pfunc(t_data *data)
{
  printf("Initialisation ptr sur fonctions pour les paquets: ");
  data->pfunc = NULL;
  add_pfunc(data, AUTH, NULL);
  add_pfunc(data, GAMER, get_pgamer);
  add_pfunc(data, MAP, get_map);
  printf("Done.\n");
}
