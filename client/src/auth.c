/*
** auth.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed May 28 12:22:37 2014 camill_n
** Last update Mon Jun  2 23:38:45 2014 camill_n
*/

#include <stdio.h>
#include "common.h"

int		get_pseudo(char *pseudo)
{
  int		ret;

  ret = 0;
  printf("Bienvenue sur Scarity !\nQuel pseudo désiré vous ?\n> ");
  fflush(stdout);
  if ((ret = read(0, pseudo, 50)) > 0)
    pseudo[ret - 1] = 0;
  return (0);
}

int		auth(t_data *data, t_paquet *paquet)
{
  t_send	*send;
  char		pseudo[50];

  bzero(pseudo, 50 * sizeof(char));
  get_pseudo(pseudo);
  send = add_send(data, data->sock, 0);
  strcat(send->paquet, AUTH);
  strcat(send->paquet + 2, pseudo);
  data->pseudo = strdup(pseudo);
  ++send->state;
  return (0);
}

int		deauth(t_data *data)
{
  t_send	*send;

  send = add_send(data, data->sock, 0);
  strcat(send->paquet, DEAUTH);
  ++send->state;
  usleep(2000);
  return (0);
}
