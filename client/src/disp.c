/*
** disp.c for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 16:00:34 2014 camill_n
** Last update Fri May 30 18:01:48 2014 camill_n
*/

#include "common.h"

void		disp_gamer(t_data *data)
{
  t_gamer	*tmp;

  tmp = data->gamer;
  printf("Il y a %d joueur(s) connecté\n", data->nb_gamer);
  while (tmp)
    {
      printf("Pseudo: %s (%f - %f)\n", tmp->pseudo, tmp->pos[0], tmp->pos[1]);
      tmp = tmp->next;
    }
}

void		disp_sock(t_data *data)
{
  /* t_sock	*tmp; */

  /* tmp = data->sock; */
  /* printf("Liste des sockets connecté:\n"); */
  /* while (tmp) */
  /*   { */
  /*     printf("Socket de type: %d\n\n", tmp->type); */
  /*     tmp = tmp->next; */
  /*   } */
}

void		disp_paquet(t_data *data)
{
  t_paquet	*tmp;

  tmp = data->paquet;
  printf("Nb paquet; %d\n", data->nb_paquet);
  while (tmp)
    {
      printf("Paquet : %s\n", tmp->cbuff);
      tmp = tmp->next;
    }
}

void		disp_map(t_data *data)
{
  int		i;
  int		j;

  i = 0;
  while (i < HEIGHT)
    {
      j = 0;
      while (j < WIDTH)
	{
	  if (data->imap->map[i * WIDTH + j] == 0)
	    printf(".");
	  else
	    printf("*");
	  ++j;
	}
      printf("\n");
      ++i;
    }
}
