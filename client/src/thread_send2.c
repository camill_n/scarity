/*
** thread_send2.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May 29 11:57:19 2014 camill_n
** Last update Thu Jun  5 01:23:52 2014 camill_n
*/

#include "common.h"

t_send		*add_send(t_data *data, t_sock *sock, int type)
{
    t_send	*tmp;
    t_send	*new_send;

    new_send = malloc(sizeof(t_send));
    bzero(new_send, sizeof(t_send);
    new_send->next = NULL;
    bzero(new_send->paquet, PAQUET_SIZE);
    new_send->type = type;
    new_send->state = 0;
    new_send->sock = sock;
    if (data->send == NULL)
      data->send = new_send;
    else
      {
        tmp = data->send;
        while (tmp->next != NULL)
	  tmp = tmp->next;
        tmp->next = new_send;
      }
    ++data->nb_send;
    return (new_send);
}

void		del_send(t_data *data, t_send *send)
{
    t_send	*tmp;

    if (!data->nb_send)
      return ;
    if (data->send != NULL && data->send->next == NULL)
      data->send = NULL;
    else if (data->send == send)
        data->send = send->next;
    else
    {
      tmp = data->send;
      while (tmp->next != send)
	tmp = tmp->next;
      if (send->next != NULL)
	tmp->next = send->next;
      else
	tmp->next = NULL;
    }
    free(send);
    --data->nb_send;
}

void            *thread_send(void *ptr_data)
{
  t_data	*data;

  data = ptr_data;
  printf("Création du thread d'envoi\n");
  while (data->run)
    {
      if (data->send && data->send->state)
	{
	  write(SOCK->sock, data->send->paquet, PAQUET_SIZE);
	  del_send(data, data->send);
	}
      usleep(100);
    }
  return (NULL);
}
