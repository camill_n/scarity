/*
** sock.c for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:22:22 2014 camill_n
** Last update Sat May 31 01:31:36 2014 camill_n
*/

#include <pthread.h>
#include "common.h"

int	connect_server(t_data *data, char *srv)
{
  bzero(&SOCK->info, sizeof(SOCK->info));
  SOCK->addr_host = inet_addr(srv);
  if ((long)SOCK->addr_host != (long)-1)
    bcopy(&SOCK->addr_host, &SOCK->info.sin_addr, sizeof(SOCK->addr_host));
  else
    {
      SOCK->host = gethostbyname(srv);
      if (SOCK->host == NULL)
	{
	  printf("Server is not reachable\n");
	  return (0);
	}
      bcopy(SOCK->host->h_addr, &SOCK->info.sin_addr, SOCK->host->h_length);
    }
  SOCK->info.sin_port = htons(2500);
  SOCK->info.sin_family = AF_INET;
  if ((SOCK->sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
      printf("Fail during the creation of socket\n");
      return (0);
    }
  if (connect(SOCK->sock, (struct sockaddr *)&SOCK->info,
	      sizeof(SOCK->info)) < 0)
    printf("Connexion refused..\n");
  else
    data->connected = 1;
  return (1);
}

int	listen_loop(t_data *data)
{
  pthread_t	t_receive;
  pthread_t	t_exec;
  pthread_t	t_send;

  printf("Démarage du serveur (PORT: %d)..\n", SCARITY_PORT);
  pthread_create(&t_receive, NULL, thread_receive, (void *)data);
  pthread_create(&t_exec, NULL, thread_exec, (void *)data);
  pthread_create(&t_send, NULL, thread_send, (void *)data);
  //pthread_join(t_receive, 0);
  while (data->run)
    sdl_loop(data);
  return (EXIT_SUCCESS);
}
