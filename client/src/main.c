/*
** main.c for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:20:15 2014 camill_n
** Last update Mon Jun  2 23:36:35 2014 camill_n
*/

#include <signal.h>
#include "common.h"

int		main(int ac, char **av)
{
  t_data	data;

  puts("Lancement du client Scarity:\n");
  init_data(&data);
  connect_server(&data, av[1]);
  init_pfunc(&data);
  auth(&data, NULL);
  putchar('\n');
  sdl_init(&data);
  init_sprite(&data);
  listen_loop(&data);
  return (EXIT_SUCCESS);
}
