/*
** cbuff.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May 22 18:51:25 2014 camill_n
** Last update Fri May 30 18:57:53 2014 camill_n
*/

#include "common.h"

t_paquet	*add_paquet(t_data *data)
{
  t_paquet	*tmp;
  t_paquet	*new_paquet;

  new_paquet = malloc(sizeof(t_paquet));
  bzero(new_paquet->cbuff, PAQUET_SIZE);
  new_paquet->state = 0;
  new_paquet->next = NULL;
  if (data->paquet == NULL)
    data->paquet = new_paquet;
  else
    {
      tmp = data->paquet;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_paquet;
    }
  ++data->nb_paquet;
  return (new_paquet);
}

void		del_paquet(t_data *data, t_paquet *paquet)
{
  t_paquet	*tmp;

  if (!data->nb_paquet)
    return ;
  if (data->paquet != NULL && data->paquet->next == NULL)
    data->paquet = NULL;
  else if (data->paquet == paquet)
    data->paquet = paquet->next;
  else
    {
      tmp = data->paquet;
      while (tmp->next != paquet)
	tmp = tmp->next;
      if (paquet->next != NULL)
	tmp->next = paquet->next;
      else
	tmp->next = NULL;
    }
  free(paquet);
  --data->nb_paquet;
}
