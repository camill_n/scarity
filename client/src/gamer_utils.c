/*
** gamer_utils.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May 29 11:52:46 2014 camill_n
** Last update Mon Jun  2 21:58:42 2014 camill_n
*/

#include "common.h"

int		get_pgamer(t_data *data, t_paquet *paquet)
{
  t_gamer	*gamer;
  t_gamer	*tmp;

  tmp = (t_gamer *)(paquet->cbuff + 2);
  gamer = data->gamer;
  while (gamer)
    {
      if (!strcmp(tmp->pseudo, gamer->pseudo))
	{
	  memcpy(gamer, tmp, sizeof(t_gamer_p));
	  return (0);
	}
      gamer = gamer->next;
    }
  gamer = add_gamer(data, "a del");
  memcpy(gamer, tmp, sizeof(t_gamer_p));
  if (!strcmp(data->pseudo, gamer->pseudo))
    data->ref_gamer = gamer;
  return (0);
}
