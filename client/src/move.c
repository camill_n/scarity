/*
** move.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May 29 12:38:44 2014 camill_n
** Last update Mon Jun  2 21:43:20 2014 camill_n
*/

#include "common.h"

int		move_action(t_data *data, t_gamer *gamer, int mode)
{
  float		new_pos[2];

  memcpy(new_pos, gamer->pos, 2 * sizeof(float));
  if (mode == 0)
    new_pos[1] -= 0.2;
  if (mode == 1)
    new_pos[1] += 0.2;
  if (mode == 2)
    new_pos[0] -= 0.2;
  if (mode == 3)
    new_pos[0] += 0.2;
  if (new_pos[0] > 0 && new_pos[0] < WIDTH &&
      new_pos[1] > 0 && new_pos[1] < HEIGHT)
    {
      memcpy(gamer->pos, new_pos, sizeof(new_pos));
      return (1);
    }
  return (0);
}

int		send_move(t_data *data, int mov)
{
  t_send	*send;

  send = add_send(data, data->sock, ALL);
  move_action(data, data->ref_gamer, mov);
  strcat(send->paquet, MOVE);
  if (mov == 0)
    strcat(send->paquet + 2, "up");
  if (mov == 1)
    strcat(send->paquet + 2, "down");
  if (mov == 2)
    strcat(send->paquet + 2, "left");
  if (mov == 3)
    strcat(send->paquet + 2, "right");
  ++send->state;
  return (0);
}
