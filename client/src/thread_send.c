/*
** thread_send.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed May 28 12:02:09 2014 camill_n
** Last update Sat May 31 01:35:58 2014 camill_n
*/

#include "common.h"

void		*thread_exec(void *ptr_data)
{
  t_data	*data;
  t_pfunc	*pfunc;
  int		run;

  data = ptr_data;
  printf("Création du thread d'execution\n");
  while (data->run)
    {
      if (data->paquet && data->paquet->state)
	{
	  pfunc = data->pfunc;
	  run = 1;
	  while (run && pfunc)
	    {
	      if (!strncmp(pfunc->cmd, data->paquet->cbuff, 2))
		{
		  if (pfunc->exec)
		    pfunc->exec(data, data->paquet);
		  --run;
		}
	      pfunc = pfunc->next;
	    }
	  del_paquet(data, data->paquet);
	}
      usleep(100);
    }
  return (NULL);
}
