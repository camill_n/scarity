/*
** thread_receive.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May 22 18:35:49 2014 camill_n
** Last update Sat May 31 01:35:48 2014 camill_n
*/


#include "common.h"

void		del_sock(t_data *data, t_sock *sock)
{
    t_sock	*tmp;

    if (!data->nb_sock)
      return ;
    if (data->sock != NULL && data->sock->next == NULL)
      data->sock = NULL;
    else if (data->sock == sock)
      data->sock = sock->next;
    else
      {
	tmp = data->sock;
	while (tmp->next != sock)
	  tmp = tmp->next;
	if (sock->next != NULL)
	  tmp->next = sock->next;
	else
	  tmp->next = NULL;
      }
    --(data->nb_sock);
    close(sock->sock);
    free(sock);
}

void		receive_paquet(t_data *data)
{
  t_paquet	*paquet;

  paquet = add_paquet(data);
  bzero(CBUFF, sizeof(CBUFF));
  read(SOCK->sock, CBUFF, PAQUET_SIZE);
  ++paquet->state;
}

void		*thread_receive(void *ptr_data)
{
  t_data	*data;

  printf("Création du thread de reception\n");
  data = ptr_data;
  if (data->connected)
    {
      while (data->run)
	{
	  receive_paquet(data);
	  usleep(100);
	}
    }
  return (NULL);
}
