/*
** init.c for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:49:39 2014 camill_n
** Last update Sun Jun  1 01:22:14 2014 camill_n
*/

#include "common.h"

void	init_data(t_data *data)
{
  printf("-> Iniatilisation de la structure data: ");
  data->sock = malloc(sizeof(t_sock));
  data->gamer = NULL;
  data->paquet = NULL;
  data->send = NULL;
  data->sprite = NULL;
  data->nb_gamer = 0;
  data->connected = 0;
  data->nb_send = 0;
  data->nb_paquet = 0;
  data->nb_sock = 0;
  data->imap = malloc(sizeof(t_imap));
  data->run = 1;
  printf("\033[32mDone.\n\033[0m");
}
