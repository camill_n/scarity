/*
** sock.c for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:22:22 2014 camill_n
** Last update Wed Jun  4 13:09:28 2014 camill_n
*/

#include <pthread.h>
#include "common.h"

int		check_socket(int sockfd)
{
  if (listen(sockfd, 1) < 0)
    {
      perror("listen");
      exit (EXIT_FAILURE);
    }
  return (0);
}

t_sock		*add_sock(t_data *data, char type)
{
    t_sock	*tmp;
    t_sock	*new_sock;

    new_sock = malloc(sizeof(t_sock));
    new_sock->type = type;
    new_sock->next = NULL;
    if (data->sock == NULL)
      data->sock = new_sock;
    else
      {
        tmp = data->sock;
        while (tmp->next != NULL)
	  tmp = tmp->next;
        tmp->next = new_sock;
      }
    ++(data->nb_sock);
    return (new_sock);
}

void		init_srv_socket(t_data *data)
{
  int		opt;
  t_sock	*sock;

  printf("-> Initialisation du socket principal: ");
  opt = 1;
  sock = add_sock(data, SOCKET_SERVER);
  bzero(&sock->info, sizeof(sock->info));
  if ((sock->sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
      perror("socket");
      exit(-1);
    }
  sock->info.sin_family = AF_INET;
  sock->info.sin_port = htons(SCARITY_PORT);
  sock->info.sin_addr.s_addr = INADDR_ANY;
  setsockopt(sock->sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
  if (bind(sock->sock, (struct sockaddr *)&sock->info,
	   sizeof(sock->info)) == -1)
    {
      perror("bind");
      exit(-1);
    }
  check_socket(sock->sock);
  printf("\033[32mDone.\033[0m\n");
}

int	listen_loop(t_data *data)
{
  int		check;
  pthread_t	t_receive;
  pthread_t	t_exec;
  pthread_t	t_send;

  check = 0;
  printf("Démarage du serveur (PORT: %d)..\n", SCARITY_PORT);
  pthread_create(&t_receive, NULL, thread_receive, (void *)data);
  pthread_create(&t_exec, NULL, thread_exec, (void *)data);
  pthread_create(&t_send, NULL, thread_send, (void *)data);
  pthread_join(t_receive, 0);
  while (!check);
  return (EXIT_SUCCESS);
}
