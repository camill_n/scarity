/*
** map.c for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue May 13 08:34:30 2014 camill_n
** Last update Tue Jun  3 00:37:14 2014 camill_n
*/

/* MAP FOR SERVER */

#include "common.h"

void     load_map(t_data *data, char *path)
{
  FILE	*file_map = NULL;
  int	i = 0;
  char	*tmp;
  char	**tab;
  int	j;

  printf("-> Initialisation de la map de jeu en cours: ");
  tmp = malloc(sizeof(char) * 400);
  file_map = fopen(path, "r");
  if (file_map == NULL)
    printf("ouverture du fichier map impossible\n");
  else
    {
      while (i < HEIGHT)
        {
	  fgets(tmp, 400, file_map);
	  tab = my_wordtab(tmp, ' ');
	  j = 0;
	  while (j < WIDTH)
            {
	      MAP_GAME[(i * WIDTH) + j] = atoi(tab[j]);
	      ++j;
            }
	  my_free_wordtab(tab);
	  ++i;
        }
      printf("\e[32mDone.\e[0m\n");
      fclose(file_map);
    }
  free(tmp);
}

int	map(t_data *data, t_paquet *paquet)
{
  t_send	*send;

  send = add_send(data, paquet->sock, USER);
  disp_map(data);
  strcat(send->paquet, MAP);
  memcpy(send->paquet + 2, data->imap->map, sizeof(data->imap->map));
  ++send->state;
  return (0);
}
