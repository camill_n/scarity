/*
** auth.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed May 28 12:22:37 2014 camill_n
** Last update Thu Jun  5 01:04:10 2014 CAMILLI
*/

#include <stdio.h>
#include "common.h"

int		auth(t_data *data, t_paquet *paquet)
{
  add_gamer(data, CBUFF + 2, paquet->sock);
  send_all_gamer(data, paquet->sock);
  return (0);
}

int		deauth(t_data *data, t_paquet *paquet)
{
  printf("Le client %s quitte la partie\n", paquet->sock->gamer->pseudo);
  del_gamer(data, paquet->sock->gamer);
  del_sock(data, paquet->sock);
  return (0);
}
