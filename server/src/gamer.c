/*
** gamers.c for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:25:14 2014 camill_n
** Last update Sun Jun  1 01:54:17 2014 camill_n
*/

#include <time.h>
#include "common.h"

t_gamer		*add_gamer(t_data *data, char *pseudo, t_sock *sock)
{
    t_gamer	*tmp;
    t_gamer	*new_gamer;

    srand(time(NULL));
    printf("Création du joueur %s\n", pseudo);
    new_gamer = malloc(sizeof(t_gamer));
    new_gamer->sock_info = sock;
    new_gamer->next = NULL;
    sock->gamer = new_gamer;
    bzero(new_gamer->pseudo, 50);
    new_gamer->pos[0] = rand() % WIDTH;
    new_gamer->pos[1] = rand() % HEIGHT;
    memcpy(new_gamer->pseudo, pseudo, strlen(pseudo));
    if (data->gamer == NULL)
      data->gamer = new_gamer;
    else
      {
        tmp = data->gamer;
        while (tmp->next != NULL)
	  tmp = tmp->next;
        tmp->next = new_gamer;
      }
    ++data->nb_gamer;
    return (new_gamer);
}

void		del_gamer(t_data *data, t_gamer *gamer)
{
    t_gamer	*tmp;

    if (!data->nb_gamer)
      return ;
    if (data->gamer != NULL && data->gamer->next == NULL)
      data->gamer = NULL;
    else if (data->gamer == gamer)
        data->gamer = gamer->next;
    else
    {
      tmp = data->gamer;
      while (tmp->next != gamer)
	tmp = tmp->next;
      if (gamer->next != NULL)
	tmp->next = gamer->next;
      else
	tmp->next = NULL;
    }
    free(gamer);
    --data->nb_gamer;
}
