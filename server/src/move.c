/*
** move.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May 29 12:38:44 2014 camill_n
** Last update Mon Jun  2 21:15:04 2014 camill_n
*/

#include "common.h"

int		move_action(t_data *data, t_gamer *gamer, int mode)
{
  float		new_pos[2];

  memcpy(new_pos, gamer->pos, 2 * sizeof(float));
  if (mode == 0)
    new_pos[1] -= 0.2;
  if (mode == 1)
    new_pos[1] += 0.2;
  if (mode == 2)
    new_pos[0] -= 0.2;
  if (mode == 3)
    new_pos[0] += 0.2;
  if (new_pos[0] > 0 && new_pos[0] < WIDTH &&
      new_pos[1] > 0 && new_pos[1] < HEIGHT)
    {
      memcpy(gamer->pos, new_pos, sizeof(new_pos));
      return (1);
    }
  return (0);
}

int		move(t_data *data, t_paquet *paquet)
{
  t_send	*send;
  int		ret;

  ret = 0;
  send = add_send(data, paquet->sock, ALL);
  if (!strcmp(CBUFF + 2, "up"))
    ret = move_action(data, paquet->sock->gamer, 0);
  if (!strcmp(CBUFF + 2, "down"))
    ret = move_action(data, paquet->sock->gamer, 1);
  if (!strcmp(CBUFF + 2, "left"))
    ret = move_action(data, paquet->sock->gamer, 2);
  if (!strcmp(CBUFF + 2, "right"))
    ret = move_action(data, paquet->sock->gamer, 3);
  if (ret)
    {
      strcat(send->paquet, GAMER);
      create_pgamer(data, send, paquet->sock->gamer);
      ++send->state;
    }
  else
    del_send(data, send);
  return (0);
}
