/*
** main.c for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:20:15 2014 camill_n
** Last update Tue Jun  3 10:04:05 2014 camill_n
*/

#include "common.h"

int		main(int ac, char **av)
{
  t_data	data;

  puts("Lancement du serveur Scarity:\n");
  init_data(&data);
  init_srv_socket(&data);
  init_pfunc(&data);
  load_map(&data, av[1]);
  putchar('\n');
  listen_loop(&data);
  return (EXIT_SUCCESS);
}
