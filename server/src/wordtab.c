/*
** wordtab.c for mysh in /home/camill_n/rendu/PSU_2013_minishell1
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Mon Dec  9 00:24:36 2013 Nicolas Camilli
** Last update Sun May 11 15:33:12 2014 camill_n
*/

#include "common.h"

void	my_show_wordtab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != NULL)
    printf("%s\n", tab[i++]);
}

int	get_nb_word(char *str, char c)
{
  int	i;
  int	cpt;

  cpt = 1;
  i = 0;
  while (str[i] != '\0')
    {
      (str[i] == ' ' && str[i + 1] != ' ') ? ++cpt : 0;
      (str[i] == c && str[i + 1] != c) ? ++cpt : 0;
      ++i;
    }
  return (cpt);
}

void	my_free_wordtab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != NULL)
    {
      free(tab[i]);
      ++i;
    }
  free(tab);
}

int	get_sizetab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != NULL)
    ++i;
  return (i);
}

char	**duptab(char **tab)
{
  int	i;
  char	**new_tab;
  int	size;

  size = get_sizetab(tab) + 1;
  new_tab = malloc(size * sizeof(char *));
  i = 0;
  while (tab[i] != NULL)
    {
      new_tab[i] = strdup(tab[i]);
      ++i;
    }
  new_tab[i] = NULL;
  return (new_tab);
}
