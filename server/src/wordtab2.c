/*
** wordtab2.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 18 19:03:13 2014 camill_n
** Last update Sun May 11 15:33:47 2014 camill_n
*/

#include "common.h"

char	*my_strncat(char *src, int i, int j)
{
  int	k;
  char	*new_str;

  k = 0;
  new_str = malloc((strlen(src) + 1) * sizeof(char));
  while (i < j)
    new_str[k++] = src[i++];
  new_str[k] = '\0';
  return (new_str);
}

char	**my_wordtab(char *av, char sep)
{
  char	**tab;
  int	nb_word;
  int	i;
  int	tmp;
  int	cpt;

  nb_word = get_nb_word(av, ':');
  tab = malloc((nb_word + 1) * sizeof(char *));
  i = 0;
  cpt = 0;
  while (av[i] != '\0')
    {
      while ((av[i] == ' ' || av[i] == '\t' || av[i] == sep) && av[i] != '\0')
        ++i;
      tmp = i;
      while (av[tmp] != '\0' && (av[tmp] != sep &&
				 av[tmp] != ' ' && av[tmp] != '\t'))
        ++tmp;
      tmp - i > 0 ? tab[cpt] = my_strncat(av, i, tmp) : 0;
      tmp - i > 0 ? ++cpt : 0;
      i = tmp;
    }
  tab[cpt] = NULL;
  return (tab);
}
