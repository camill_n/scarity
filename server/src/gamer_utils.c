/*
** gamer_utils.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May 29 11:52:46 2014 camill_n
** Last update Thu Jun  5 01:23:12 2014 CAMILLI
*/

#include "common.h"

int	create_pgamer(t_data *data, t_send *send, t_gamer *gamer)
{
  t_gamer_p	*tmp;

  tmp = (t_gamer_p *)gamer;
  memcpy(send->paquet + 2, tmp, sizeof(t_gamer_p));
  return (0);
}

int	send_all_gamer(t_data *data, t_sock *sock)
{
  t_send	*send;
  t_gamer	*gamer;

  gamer = data->gamer;
  while (gamer)
    {
      send = add_send(data, sock, USER);
      memcpy(send->paquet, GAMER, 2);
      create_pgamer(data, send, gamer);
      ++send->state;
      gamer = gamer->next;
    }
  return (0);
}
