/*
** thread_receive.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May 22 18:35:49 2014 camill_n
** Last update Tue Jun  3 10:10:53 2014 camill_n
*/


#include "common.h"

void	add_client(t_data *data, fd_set *rdfs)
{
  t_sock	*sock;
  int		size;

  printf("Détection d'un nouveau client\n");
  sock = add_sock(data, SOCKET_CLIENT);
  bzero(&sock->info, sizeof(sock->info));
  size = 0;
  sock->sock = accept(data->sock->sock, (struct sockaddr *)&sock->info,
		      (socklen_t *)&size);
  if (sock->sock == -1)
    {
      perror("socket");
      exit(-1);
    }
  disp_gamer(data);
}

void		del_sock(t_data *data, t_sock *sock)
{
    t_sock	*tmp;

    if (!data->nb_sock)
      return ;
    if (data->sock != NULL && data->sock->next == NULL)
      data->sock = NULL;
    else if (data->sock == sock)
      data->sock = sock->next;
    else
      {
	tmp = data->sock;
	while (tmp->next != sock)
	  tmp = tmp->next;
	if (sock->next != NULL)
	  tmp->next = sock->next;
	else
	  tmp->next = NULL;
      }
    --(data->nb_sock);
    close(sock->sock);
    free(sock);
}

void		get_higher_sock(t_data *data, int *hsock)
{
  t_sock	*sock;

  *hsock = data->sock->sock;
  sock = data->sock;
  while (sock)
    if (sock->sock > *hsock)
      (*hsock = sock->sock);
    else
      (sock = sock->next);
}

void		receive_paquet(t_data *data, t_sock *sock)
{
  t_paquet	*paquet;
  int		ret;

  paquet = add_paquet(data, sock);
  bzero(CBUFF, sizeof(CBUFF));
  ret = read(sock->sock, CBUFF, sizeof(CBUFF));
  CBUFF[ret - 1] = 0;
  ++paquet->state;
}

void		check_sock(t_data *data, fd_set *rdfs)
{
  t_sock	*sock;

  sock = data->sock;
  while (sock)
    {
      if (FD_ISSET(sock->sock, rdfs))
	{
	  if (sock->type == SOCKET_SERVER)
	    add_client(data, rdfs);
	  else
	    receive_paquet(data, sock);
	}
      sock = sock->next;
    }
}

void		*thread_receive(void *ptr_data)
{
  t_sock	*sock;
  fd_set	rdfs;
  int		highest_sock;
  t_data	*data;

  printf("Création du thread de reception\n");
  data = ptr_data;
  highest_sock = data->sock->sock;
  while (data->thread_receive)
    {
      get_higher_sock(data, &highest_sock);
      FD_ZERO(&rdfs);
      sock = data->sock;
      while (sock)
	{
	  FD_SET(sock->sock, &rdfs);
	  sock = sock->next;
	}
      if (select(highest_sock + 1, &rdfs, NULL, NULL, NULL) < 0)
	{
	  perror("select");
	  exit(EXIT_FAILURE);
	}
      check_sock(data, &rdfs);
      usleep(100);
    }
  return (NULL);
}
