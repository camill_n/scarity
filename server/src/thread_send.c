/*
** thread_send.c for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed May 28 12:02:09 2014 camill_n
** Last update Mon Jun  2 21:22:19 2014 camill_n
*/

#include "common.h"

void		send_to_all(t_data *data, t_send *send)
{
  t_sock	*sock;

  sock = data->sock;
  while (sock)
    {
      if (send->sock != sock && sock->type == SOCKET_CLIENT)
	write(sock->sock, send->paquet, PAQUET_SIZE);
      sock = sock->next;
    }
}

void		*thread_exec(void *ptr_data)
{
  t_data	*data;
  t_pfunc	*pfunc;
  int		run;

  data = ptr_data;
  printf("Création du thread d'execution\n");
  while (data->thread_exec)
    {
      if (data->paquet && data->paquet->state)
	{
	  pfunc = data->pfunc;
	  run = 1;
	  while (run && pfunc)
	    {
	      if (!strncmp(pfunc->cmd, data->paquet->cbuff, 2))
		{
		  pfunc->exec(data, data->paquet);
		  --run;
		}
	      pfunc = pfunc->next;
	    }
	  del_paquet(data, data->paquet);
	}
      usleep(100);
    }
  return (NULL);
}
