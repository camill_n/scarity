/*
** struct.h for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:35:35 2014 camill_n
** Last update Thu Jun  5 01:20:48 2014 CAMILLI
*/

#ifndef STRUCT_H_
# define STRUCT_H_

#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>

# define MAP_GAME data->imap->map

typedef struct sockaddr_in	t_sock_in;
typedef struct s_gamer		t_gamer;
typedef struct s_pfunc		t_pfunc;

# define WIDTH 85
# define HEIGHT 45
# define PAQUET_SIZE 72 * sizeof(char)
# define CBUFF paquet->cbuff
# define ALL 0
# define USER 1
# define AUTH "01"
# define MOVE "02"
# define GAMER "03"
# define DEAUTH "04"
# define MAP "05"

typedef struct		s_imap
{
  char			map[HEIGHT * WIDTH];
}			t_imap;

typedef struct		s_sock
{
  char			type;
  int			sock;
  t_sock_in		info;
  t_gamer		*gamer;
  struct s_sock		*next;
}			t_sock;

typedef struct		s_gamer
{
  char			pseudo[50];
  float			pos[2];
  t_sock		*sock_info;
  struct s_gamer	*next;
}			t_gamer;

typedef struct		s_gamer_p
{
  char			pseudo[50];
  float			pos[2];
}			t_gamer_p;

typedef struct		s_paquet
{
  int			state;
  char			cbuff[PAQUET_SIZE];
  t_sock		*sock;
  struct s_paquet	*next;
}			t_paquet;

typedef struct		s_send
{
  int			type;
  int			state;
  t_sock		*sock;
  char			paquet[PAQUET_SIZE];
  struct s_send		*next;
}			t_send;

typedef struct		s_data
{
  t_sock		*sock;
  t_gamer		*gamer;
  t_imap		*imap;
  t_paquet		*paquet;
  t_pfunc		*pfunc;
  t_send		*send;
  int			nb_gamer;
  int			nb_paquet;
  int			nb_send;
  int			nb_sock;
  int			thread_receive;
  int			thread_exec;
  int			thread_send;
}			t_data;

typedef struct		s_pfunc
{
  char			*cmd;
  int			(*exec)(t_data *data, t_paquet *paquet);
  struct s_pfunc	*next;
}			t_pfunc;

#endif
