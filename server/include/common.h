/*
** common.h for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:31:55 2014 camill_n
** Last update Thu May 22 18:45:35 2014 camill_n
*/

#ifndef COMMON_H_
# define COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "wordtab.h"
#include "struct.h"
#include "server.h"
#include "sock.h"
#include "header.h"
#include "thread.h"

#endif
