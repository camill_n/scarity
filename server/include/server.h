/*
** server.h for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:27:07 2014 camill_n
** Last update Thu Jun  5 01:03:50 2014 CAMILLI
*/

#ifndef SCARITY_H_
# define SCARITY_H_

# define SCARITY_PORT 2500

// ---- FONCTIONS D'INIT ---- //

void	init_data(t_data *data);
void	load_map(t_data *data, char *path);

// ---- FONCTION GAMER ---- //

t_gamer	*add_gamer(t_data *data, char *pseudo, t_sock *sock);
void	del_gamer(t_data *data, t_gamer *gamer);
int	create_pgamer(t_data *data, t_send *send, t_gamer *gamer);
int	send_all_gamer(t_data *data, t_sock *sock);

// ---- FONCTION DISPLAY ---- //

void	disp_gamer(t_data *data);
void	disp_sock(t_data *data);
void	disp_map(t_data *data);
void	disp_buff(t_data *data);
void	disp_paquet(t_data *data);

// ---- FONCTION RECEIVE DE PACKET ---- //

void	receive_req(t_data *data, t_sock *client);

void	init_pfunc(t_data *data);
int	auth(t_data *data, t_paquet *paquet);
int	deauth(t_data *data, t_paquet *paquet);
int	move(t_data *data, t_paquet *paquet);
int	map(t_data *data, t_paquet *paquet);

#endif
