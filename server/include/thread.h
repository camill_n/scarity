/*
** thread.h for scarity_server in /home/camill_n/scaritysave/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May 22 18:44:16 2014 camill_n
** Last update Mon Jun  2 21:21:57 2014 camill_n
*/

#ifndef THREAD_H_
# define THREAD_H_

void		*thread_receive(void *ptr_data);
void		*thread_exec(void *ptr_data);
void		*thread_send(void *ptr_data);
void		send_to_all(t_data *data, t_send *send);
t_paquet	*add_paquet(t_data *data, t_sock *sock);
void		del_paquet(t_data *data, t_paquet *paquet);
t_send		*add_send(t_data *data, t_sock *sock, int type);
void		del_send(t_data *data, t_send *send);

#endif
