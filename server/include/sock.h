/*
** sock.h for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:24:33 2014 camill_n
** Last update Mon May 12 19:23:46 2014 camill_n
*/

#ifndef SOCK_H_
# define SOCK_H_

# define SOCKET_SERVER 0
# define SOCKET_CLIENT 1

void	init_srv_socket(t_data *data);
void	add_client(t_data *data, fd_set *rdfs);
int	check_socket(int sockfd);
int	listen_loop(t_data *data);
t_sock	*add_sock(t_data *data, char type);
void	del_sock(t_data *data, t_sock *sock);

#endif
