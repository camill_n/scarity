/*
** wordtab.h for wordtab in /home/camill_n/rendu/PSU_2013_minishell1
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu Jan 23 14:09:01 2014 camill_n
** Last update Sat May 10 16:20:52 2014 Antonin Bouscarel
*/

#ifndef WORDTAB_H_
# define WORDTAB_H_

int	get_nb_word(char *str, char c);
void	my_free_wordtab(char **tab);
void	my_show_wordtab(char **tab);
int	get_sizetab(char **tab);
char	**my_wordtab(char *req, char sep);
char	**duptab(char **tab);
char	*my_strncat(char *src, int i, int j);

#endif
